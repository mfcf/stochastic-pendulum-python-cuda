"""
 The Stochastic potential well with driving and red noise
 This script is a tutorial script for stochastic integration and
 visualization using red noise realized using the method of Bartosch
 the governing ODEs of the system are
 x'=v
 v'=-V'(x)-damping*v+f_deterministic+f_stochastic
 where V(x) is the potential of interest.
 A tilted double well potential is implemented in this code
"""
import numpy as np
from numpy import pi
from matplotlib import pyplot as plt
from time import perf_counter

## This section is all the book keeping for initialization
# Initialize the random number generator and seed it
random_state = 1  # const for reproducibility.
rng = np.random.RandomState(random_state)

# Specify the global parameters for the pendulum
x0deep = -3.0
x0shal = 3.0
wellslope = 2.0
dxnum = 1e-8

# Specify the time step and stochastic parameters
numsteps = 100  # sets how many time steps between saves
numouts = 2000  # sets the number of outputs
numens = 128  # sets the number of ensemble members
dt = 1e-3  # sets the time step.  Stochastic methods are low order so small time steps are required

# The parameters for the red noise as realized by Bartosch's algorithm
memt = 0.2  # sets the memory of the noise.  Need at least ten time steps in memt
rho = np.exp(-dt / memt).astype(np.float32)
rhoc = np.sqrt(1.0 - rho * rho).astype(np.float32)  # parameters for Bartosch's method

myV = lambda xnow: ((xnow - x0deep) * (xnow - x0shal)) ** 2.0 + wellslope * xnow

def do_steps(x, v, t, prevrand, mean=0.0, stddev=20.0, numsteps=100, numens=100):
    dt = np.float32(1e-3)
    t = np.float32(t)
    dxnum = np.float32(1e-6)
    wellslope = np.float32(2.0)
    pi = np.float32(3.141592654)
    period = np.float32(2.0)
    dt = np.float32(1e-3)
    damp = np.float32(8e-1)
    x0deep = np.float32(-3.0)
    x0shal = np.float32(3.0)
    amp = np.float32(15.0)

    myVp = lambda xnow: (myV(xnow + dxnum) - myV(xnow - dxnum)) / (2 * dxnum)
    myforce = lambda t: amp * np.sin(2.0 * pi * t / period)

    for _ in range(numsteps):
        nowrand = rng.normal(
            mean, stddev, size=numens
        ).astype(np.float32)  # generate a new bunch of random numbers

        # use Bartosch's memory to get the right "memory"
        pertnow = np.float32(rhoc) * nowrand + np.float32(rho) * prevrand

        # Store for weighting in next time-iteration.
        prevrand = pertnow.copy()

        # now define the rhs for the DE
        rhth = v
        rhv = -myVp(x) - damp * v + pertnow + myforce(t) * np.ones((numens,), dtype=np.float32)

        # step the DE forward if g had a white noise component this would
        # get trickier to account for the sqrt(dt) in Ito's calculus
        x += dt * rhth
        v += dt * rhv
        t += dt

    return x, v, t, prevrand

mysig = 20.0  # set the standard deviation of the noise
prevrand = np.zeros((numens,), dtype=np.float32)  # start g at g0

mu = 0.0

# Initial conditions
x = x0shal * np.ones((numens,), dtype=np.float32)  # start near the shallow well location
# x=1e-4*pi*ones((numens,))   # start near the stable state
v = np.zeros((numens,), dtype=np.float32)  # initially assume zero velocity
t = 0.0

# Set up arrays for storage and store initial state
xs = np.zeros((numouts + 1, numens), dtype=np.float32)
vs = np.zeros((numouts + 1, numens), dtype=np.float32)
ts = np.zeros((numouts + 1, 1), dtype=np.float32)
xs[0, :] = x
vs[0, :] = v
ts[0] = t

NO_DATA = np.iinfo(np.int64).min

nowrand = np.empty((numens, 1), dtype=np.float32)

threads_per_block = 32
blocks = int(np.ceil(numens/threads_per_block))  # 256.


# The main work horse loops.  The outer loop can be used view to intermediate results
start_time = perf_counter()
for ii in range(numouts + 1):
    x, v, t, prevrand = do_steps(x, v, t, prevrand, mean=mu, stddev=mysig, numsteps=numsteps, numens=numens)


    # Store outputs regularly for post-processing/plotting/analysis.
    xs[ii, :] = x.copy()
    vs[ii, :] = v.copy()
    ts[ii] = np.float32(t)

end_time = perf_counter()
duration = end_time - start_time
print(f"Elapsed time for outer loop: {duration:04f} seconds")

# Now that we have the data we can make a variety of pictures
plt.figure(1)
plt.clf()

plt.subplot(2, 1, 1)
plt.plot(ts, xs[:, 1])
plt.xlabel("t")
plt.ylabel("x")
plt.title("one realization")
plt.subplot(2, 1, 2)
plt.plot(ts, vs[:, 1])
plt.xlabel("t")
plt.ylabel("v")

plt.figure(2)
plt.clf()
plt.subplot(2, 1, 1)
plt.plot(xs[:, 1], vs[:, 1])
plt.ylabel("v")
plt.xlabel("x")
plt.title("one phase space plot")
plt.subplot(2, 1, 2)
plt.plot(np.mean(xs, axis=1), np.mean(vs, axis=1))
plt.ylabel("v")
plt.xlabel("x")
plt.title("mean phase space plot")

plt.figure(3)
plt.clf()
plt.subplot(2, 1, 1)
plt.plot(ts, np.std(xs, axis=1))
plt.ylabel("standard deviation of x")
plt.xlabel("t")
plt.subplot(2, 1, 2)
plt.plot(ts, np.std(vs, axis=1))
plt.ylabel("standard deviation of v")
plt.xlabel("t")

plt.figure(4)
plt.clf()
plt.subplot(2, 1, 1)
mnth = np.min(xs[-1, :])
mxth = np.max(xs[-1, :])
bins = np.linspace(mnth, mxth, int(np.floor(numens / 20) + 1))
plt.hist(xs[-1, :], bins)
plt.title("histogram of xs final time")
plt.subplot(2, 1, 2)
mnv = min(vs[-1, :])
mxv = max(vs[-1, :])
bins = np.linspace(mnv, mxv, int(np.floor(numens / 20) + 1))
plt.hist(vs[-1, :], bins)
plt.title("histogram of vs final time")

# now compute waiting times
waits = []
# set how close to the unstable equilibrium is close enough
myeps = 0.1
# now find the unstable equilibrium point
xnow = np.linspace(-1.0, 1.0, 1001)
vv = myV(xnow)
aa = np.max(xnow)
bb = np.where(xnow == aa)[0]
xtop = xnow[bb]
for ii in range(numens):
    xnow = xs[:, ii]
    crossings = np.where(abs(xnow - xtop) < myeps)[0]
    # find the crossings and make sure the set isn't empty
    if len(crossings) > 0:
        waitnow = []
        waitnow.append(crossings[0])
        waitnow = np.array(waitnow)
        dummy = np.diff(crossings)
        tooshorts = np.where(dummy < 2)[0]
        # only keep data if we're not fidgeting around the equilibrium
        dummy[[ts for ts in tooshorts]] = NO_DATA
        waitnow = np.concatenate(
            (waitnow, np.array([d for d in dummy if d != NO_DATA]))
        )
        waits = np.concatenate((waits, np.array([wn for wn in waitnow])))


# Scale.
waits = np.array(waits) * dt * numsteps
period = np.float32(2.0)

plt.figure(5)
plt.clf()

plt.subplot(2, 1, 1)
plt.hist(waits, 50)
plt.title("histogram of waiting times")
plt.subplot(2, 1, 2)
plt.hist(waits, np.linspace(0.0, 3 * period, 51))
plt.title("subrange")
plt.axis([0, 3 * period - 0.5, 0, 7e2])

plt.show()
